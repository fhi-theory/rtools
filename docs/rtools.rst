rtools package
==============

Subpackages
-----------

.. toctree::

    rtools.cheminformatics
    rtools.convergence
    rtools.cube
    rtools.filesys
    rtools.fitting
    rtools.geometry
    rtools.helpers
    rtools.mapping
    rtools.misc
    rtools.submitagents
    rtools.tumcd

Submodules
----------

rtools.units module
-------------------

.. automodule:: rtools.units
    :members:
    :undoc-members:
    :show-inheritance:


Module contents
---------------

.. automodule:: rtools
    :members:
    :undoc-members:
    :show-inheritance:
