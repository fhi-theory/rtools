rtools.geometry package
=======================

Submodules
----------

rtools.geometry.curve module
----------------------------

.. automodule:: rtools.geometry.curve
    :members:
    :undoc-members:
    :show-inheritance:


Module contents
---------------

.. automodule:: rtools.geometry
    :members:
    :undoc-members:
    :show-inheritance:
