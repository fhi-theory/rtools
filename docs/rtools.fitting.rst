rtools.fitting package
======================

Submodules
----------

rtools.fitting.birchmurnaghan module
------------------------------------

.. automodule:: rtools.fitting.birchmurnaghan
    :members:
    :undoc-members:
    :show-inheritance:


Module contents
---------------

.. automodule:: rtools.fitting
    :members:
    :undoc-members:
    :show-inheritance:
